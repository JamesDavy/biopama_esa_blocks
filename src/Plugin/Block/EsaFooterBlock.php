<?php

/**
 * @file
 */
namespace Drupal\biopama_esa_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Creates an ESA Footer Block
 * @Block(
 * id = "block_esa_footer",
 * admin_label = @Translation("ESA Footer block"),
 * )
 */
class EsaFooterBlock extends BlockBase implements BlockPluginInterface{

    /**
     * {@inheritdoc}
     */
    public function build() {
        return array (
			'#theme' => 'esa_footer',
        );
    }

}