<?php

/**
 * @file
 */
namespace Drupal\biopama_esa_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Creates an ESA Header Block
 * @Block(
 * id = "block_esa_header",
 * admin_label = @Translation("ESA Header block"),
 * )
 */
class EsaHeaderBlock extends BlockBase implements BlockPluginInterface{

    /**
     * {@inheritdoc}
     */
    public function build() {
        return array (
			'#theme' => 'esa_header',
        );
    }

}